Sentiment Analysis with Encoder Transformers (BERT) on Amazon Product Reviews

1. To perform sentiment analysis on the Amazon Product Reviews, we provide the python scripts 'main.py' and 'bertutils.py'. Multiple dependencies are needed to run this scripts. To this regard, we provide a 'sentimentanalysis.yml' file that can be used to install all the needed libraries, e.g., via creating an anaconda environment than can subsequently be set to be the project's interpreter. 

2. Additionally, we also provide the distilbert.ipynb jupyter notebook that can be used to perform the sentiment analysis on, e.g., Kaggle, which allows to fit the BERT model with the help of a GPU.

3. To be able to reproduce the results we obtained when evaluating the model on the test set, as well as the predictions done by the model on the 'made_up_reviews' (see make_predictions() on 'bertutils.py'), please run the python scripts with the trained instance of the model we provide at the link down below: 

	Model: https://drive.google.com/drive/folders/1J-hDAyK4vcwK_fJ5tk_NUKQVuh6okIfa?usp=sharing

Or upload the trained model to, e.g., Kaggle, and adapt the file path to load the model in the jupyter notebook.

We performed several runs to fit the BERT model and the version provided at the mentioned link above, gave us the best results. 
