import math
import random
import pickle
import pandas as pd
import tensorflow as tf

from gensim import utils
from os.path import exists
from sklearn.model_selection import train_test_split
from transformers import DistilBertTokenizerFast
from transformers import TFDistilBertForSequenceClassification
from tensorflow.keras import backend as K


def print_data_stats(data):
    pos_reviews, neg_reviews = 0, 0
    for index, row in data.iterrows():
        if row['sentiment'] == 1:
            pos_reviews += 1
        elif row['sentiment'] == 0:
            neg_reviews += 1

    print(f'# of positive reviews: {pos_reviews}')
    print(f'# of negative reviews: {neg_reviews}')


def precision(y_true, y_pred):
    pred = tf.keras.backend.argmax(y_pred, axis=1)
    num_cols = len(pred)
    pred_f = K.cast(pred, K.floatx())
    pred_r = tf.keras.backend.reshape(
        pred_f,
        shape=(num_cols, 1)
    )
    true_pos = K.sum(K.round(K.clip(y_true * pred_r, 0, 1)))
    pred_pos = K.sum(K.round(K.clip(pred_r, 0, 1)))
    return true_pos / (pred_pos + K.epsilon())


def recall(y_true, y_pred):
    pred = tf.keras.backend.argmax(y_pred, axis=1)
    num_cols = len(pred)
    pred_f = K.cast(pred, K.floatx())
    pred_r = tf.keras.backend.reshape(
        pred_f,
        shape=(num_cols, 1)
    )
    true_pos = K.sum(K.round(K.clip(y_true * pred_r, 0, 1)))
    poss_pos = K.sum(K.round(K.clip(y_true, 0, 1)))
    return true_pos / (poss_pos + K.epsilon())


def f1_score(y_true, y_pred):
    p_m = precision(y_true, y_pred)
    r_m = recall(y_true, y_pred)
    return 2 * ((p_m * r_m) / (p_m + r_m + K.epsilon()))


def train_model(train_dataset, valid_dataset):
    # Instantiate model
    # We set num_labels=2 since we want to create a DistilBERT model instance with a sequence classification
    # head added on top of the encoder with an output size of two, i.e., with 2 classes corresponding to our
    # 2 sentiment labels: 'positive' and 'negative'.
    model = TFDistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased', num_labels=2)

    # Compile model: set optimizer, loss function and metrics.
    # - Optimizer: we use an Adam optimizer with a learning_rate=2e-5 based on this paper:
    #   https://www.researchgate.net/publication/362675821_Online_News_Sentiment_Classification_Using_DistilBERT
    # - Loss function: we use keras' cross-entropy loss for binary (0 or 1) classification.
    # - Metrics to evaluate: accuracy, precision, recall, and f1 score (we have an imbalanced dataset).
    print('-------------------------------------------------------------------------------------------------')
    print('Compiling model...')
    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=2e-5),
        loss=tf.keras.losses.BinaryCrossentropy(),
        metrics=[
            'accuracy',
            precision,
            recall,
            f1_score
        ]
    )

    # Fit model
    print('-------------------------------------------------------------------------------------------------')
    print('Fitting model...')
    batch_size = 16
    model.fit(
        train_dataset.shuffle(100).batch(batch_size),
        batch_size=batch_size,
        epochs=2,
        validation_data=valid_dataset.shuffle(100).batch(batch_size)
    )

    # Save model for faster evaluation on test set.
    print('-------------------------------------------------------------------------------------------------')
    print('Save trained model')
    model.save_pretrained('bert/model/distilbert-base-uncased')

    return model


def evaluate_model(model, test_t, test_l):
    # Instantiate corresponding tokenizer model and get encodings
    tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
    test_encodings = tokenizer(
        test_t,
        truncation=True,
        padding=True
    )

    # Create TF Test Dataset for evaluation
    test_dataset = tf.data.Dataset.from_tensor_slices((
        dict(test_encodings),
        test_l
    ))

    # Compile model
    print('Compiling model...')
    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=2e-5),
        loss=tf.keras.losses.BinaryCrossentropy(),
        metrics=[
            'accuracy',
            precision,
            recall,
            f1_score
        ]
    )

    # Evaluate model on the test set
    print('Evaluating model...')
    batch_size = 16
    result = model.evaluate(
        test_dataset.shuffle(100).batch(batch_size),
        return_dict=True,
        batch_size=batch_size
    )
    print(f'Evaluation result: {result}')

    return model


def make_predictions(model):
    made_up_reviews = [
        "This is an excellent product!",
        "I love it!!!",
        "The tablet is ok and does the job.",
        "Not as good as I imagined it would be.",
        "Not so sure if I like it",
        "This is terrible. Don't buy it.",
        "The worst batteries I've ever purchased!"
    ]

    # Instantiate corresponding tokenizer model and get encodings
    tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

    # Tokenize text
    encodings = tokenizer(
        made_up_reviews,
        truncation=True,
        padding=True
    )

    # Transform to tf.Dataset
    dataset = tf.data.Dataset.from_tensor_slices(
        dict(encodings)
    )

    # Predict sentiment classes
    predictions = model.predict(dataset.batch(1)).logits

    # Transform prediction logits to probabilities ([0,1]) with softmax
    probabilities = tf.nn.softmax(predictions, axis=1)

    # Print output probabilities for each made up review
    prob_dict = {}
    [prob_dict.update({index: entry}) for index, entry in enumerate(probabilities)]
    print(f'Predicted probabilities: {probabilities}')

    # Print mappings 'review': 'sentiment'
    sentiments = ['negative', 'positive']
    predictions = {}
    threshold = 0.5
    for index, review in enumerate(made_up_reviews):
        probs = prob_dict[index].numpy()
        class_i = probs.argmax(axis=0)
        if probs[class_i] > threshold:
            predictions.update({review: sentiments.__getitem__(class_i)})
        else:
            # if the predicted probability for a given class is <= threshold,
            # we label the prediction as 'undecided'
            predictions.update({review: 'undecided'})

    print(f'Predictions: {predictions}')

    return


def save_datasets(datasets, reviews):
    file_paths = ['bert/datasets/train_t.pkl', 'bert/datasets/train_l.pkl',
                  'bert/datasets/valid_t.pkl', 'bert/datasets/valid_l.pkl',
                  'bert/datasets/test_t.pkl', 'bert/datasets/test_l.pkl',
                  'bert/datasets/datasetfinal.pkl']

    for i, file_path in enumerate(file_paths):
        with open(file_path, 'wb') as f:
            if i == len(file_paths) - 1:
                pickle.dump(reviews, f)
            else:
                pickle.dump(datasets[i], f)


def load_dataset(file_path):
    with open(file_path, 'rb') as f:
        return pickle.load(f)


def make_datasets(data_frames):
    for index, df in enumerate(data_frames):
        print('----------------------------------------------------')
        print(f'Statistics for data frame: {index}')
        print_data_stats(df)

    if exists('bert/datasets/datasetfinal.pkl'):
        train_t = load_dataset('bert/datasets/train_t.pkl')
        train_l = load_dataset('bert/datasets/train_l.pkl')
        valid_t = load_dataset('bert/datasets/valid_t.pkl')
        valid_l = load_dataset('bert/datasets/valid_l.pkl')
        test_t = load_dataset('bert/datasets/test_t.pkl')
        test_l = load_dataset('bert/datasets/test_l.pkl')
    else:
        # Split data into 'positive'(1) and 'negative'(0) reviews.
        pos_reviews, neg_reviews = [], []
        for df in data_frames:
            pos_reviews.append([row for index, row in df.iterrows() if row[3] == 1])
            neg_reviews.append([row for index, row in df.iterrows() if row[3] == 0])

        # Remove duplicates
        print('----------------------------------------------------')
        print('Remove duplicates in data...')
        pos_reviews_no_duplicates = remove_duplicates(pos_reviews)
        neg_reviews_no_duplicates = remove_duplicates(neg_reviews)
        print(f'Debug: # positive reviews no duplicates: {len(pos_reviews_no_duplicates)}')
        print(f'Debug: # negative reviews no duplicates: {len(neg_reviews_no_duplicates)}')

        # Randomly sample 5,000 positive ratings
        random.Random(1).shuffle(pos_reviews_no_duplicates)
        pos_reviews_no_duplicates_f = [pos_reviews_no_duplicates.__getitem__(i) for i in range(5000)]

        # Combine pos and neg in one single list
        reviews = []
        [reviews.append(review_n) for review_n in neg_reviews_no_duplicates]
        [reviews.append(review_p) for review_p in pos_reviews_no_duplicates_f]
        random.Random(1).shuffle(reviews)
        print(f'Debug: total # of reviews: {len(reviews)}')

        # Extract text and sentiment labels from rows into lists for tokenizer
        reviews_text = [row[1] for row in reviews]
        reviews_labels = [row[3] for row in reviews]

        # Get sentence max_length in dataset for stats
        max_length = max([len(text) for text in reviews_text])
        print(f'Max sentence length for a review in dataset: {max_length}')

        # Split data into train and validation sets (train: 80%, valid 20%)
        print('----------------------------------------------------')
        print(f'Split data into train and valid_test datasets...')
        train_t, valid_test_t, train_l, valid_test_l = train_test_split(
            reviews_text,
            reviews_labels,
            test_size=0.2,
            train_size=0.8,
            random_state=1
        )
        print(f'Size train set t: {len(train_t)}')
        print(f'Size train set l: {len(train_l)}')

        # Split validation data into valid and test sets (valid: 50%, test: 50%)
        print('----------------------------------------------------')
        print(f'Split valid_test dataset into valid and test datasets...')
        valid_t, test_t, valid_l, test_l = train_test_split(
            valid_test_t,
            valid_test_l,
            test_size=0.5,
            train_size=0.5,
            random_state=1
        )
        print(f'Size valid set t: {len(valid_t)}')
        print(f'Size valid set l: {len(valid_l)}')
        print(f'Size test set t: {len(test_t)}')
        print(f'Size test set l: {len(test_l)}')

        # Save datasets as .pkl files
        save_datasets(
            [train_t, train_l, valid_t, valid_l, test_t, test_l],
            reviews
        )

    # Instantiate corresponding tokenizer model and get encodings
    print('----------------------------------------------------')
    print('Instantiate tokenizer for DistilBert...')
    tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
    train_encodings = tokenizer(
        train_t,
        truncation=True,
        padding=True
    )
    valid_encodings = tokenizer(
        valid_t,
        truncation=True,
        padding=True
    )

    # Create TF Datasets for (TensorFlow) transformer
    print('----------------------------------------------------')
    print('Create TF Datasets for DistilBert...')
    train_dataset = tf.data.Dataset.from_tensor_slices((
        dict(train_encodings),
        train_l
    ))
    valid_dataset = tf.data.Dataset.from_tensor_slices((
        dict(valid_encodings),
        valid_l
    ))

    return train_dataset, valid_dataset, test_t, test_l


def remove_duplicates(target):
    joined_l = [item for l_i in target for item in l_i]
    no_duplicates = dict()
    for i, item in enumerate(joined_l):
        no_duplicates[item['reviews.text']] = i

    indexes = [v for k, v in no_duplicates.items()]
    no_duplicates_l = [joined_l.__getitem__(i) for i in indexes]
    assert (len(indexes) == len(no_duplicates_l))

    return no_duplicates_l


def remove_nan_values(data):
    for col in data:
        if data[col].isnull().values.any():
            print("Debug: found nan(s)")
            nan_indexes = None
            if col == 'reviews.title':
                # fill title with empty string ''
                nan_indexes = [i for i, row in data.iterrows() if not isinstance(row[2], str)]
                data[col] = data[col].apply(lambda item: '' if not isinstance(item, str) else item)
            else:
                # remove all entries with no rating or review
                if col == 'reviews.rating':
                    nan_indexes = [i for i, row in data.iterrows() if math.isnan(row[0])]
                elif col == 'reviews.text':
                    nan_indexes = [i for i, row in data.iterrows() if not isinstance(row[1], str)]

                if nan_indexes is not None:
                    data.drop(nan_indexes, inplace=True)

            print(f"Debug: nan indexes: {nan_indexes}")
            assert (not data[col].isnull().values.any())


def preprocess(file_path):
    reviews_df = pd.read_csv(file_path)
    reviews_df = reviews_df[['reviews.rating', 'reviews.text', 'reviews.title']]

    remove_nan_values(reviews_df)

    #  Map ratings 1-3: 'negative' and 4-5: 'positive'. We only have two sentiment labels, i.e., we fine-tune our
    #  BERT model for a binary classification task, where 'negative' corresponds to class 0 and 'positive' to class 1.
    #  We map ratings 1-3 to a negative sentiment to compensate for the low % of data points with this ratings present
    #  in our dataset (all ratings are predominately 4 or 5).
    reviews_df['sentiment'] = reviews_df['reviews.rating'].apply(lambda score: 'positive' if score > 3 else 'negative')

    # Subsequently map 'negative': 0 and 'positiv': 1.
    reviews_df['sentiment'] = reviews_df['sentiment'].map({'positive': 1, 'negative': 0})

    # convert text to unicode
    for index, row in reviews_df.iterrows():
        reviews_df.loc[index, 'reviews.text'] = utils.to_unicode(row['reviews.text'])
        reviews_df.loc[index, 'reviews.title'] = utils.to_unicode(row['reviews.title'])

    return reviews_df


def load_and_preprocess_data():
    file_paths = ['bert/data/Datafiniti_Amazon_Consumer_Reviews_of_Amazon_Products_May19.csv',
                  'bert/data/Datafiniti_Amazon_Consumer_Reviews_of_Amazon_Products.csv',
                  'bert/data/1429_1.csv']

    data_files = ['bert/data/Datafiniti_Amazon_Consumer_Reviews_of_Amazon_Products_May19.pkl',
                  'bert/data/Datafiniti_Amazon_Consumer_Reviews_of_Amazon_Products.pkl',
                  'bert/data/1429_1.pkl']

    # Read & pre-process or load data if it already exists as .pkl file
    data_frames = []
    for index, file in enumerate(data_files):
        data = None
        if exists(file):
            print('Reading data file: {}'.format(file))
            data = pd.read_pickle(file)

        elif data is None:
            file_path = file_paths.__getitem__(index)
            print('Pre-processing data: {}'.format(file_path))
            data = preprocess(file_path)

            print('Saving data to pkl file: {}'.format(file))
            data.to_pickle(file)

        data_frames.append(data)

    return data_frames
