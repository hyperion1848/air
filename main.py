from bert import bertutils
from os.path import exists
from transformers import TFDistilBertForSequenceClassification


def sentiment_analysis_BERT():
    # 1. Load and pre-process dataset
    # Dataset: https://www.kaggle.com/datasets/datafiniti/consumer-reviews-of-amazon-products
    # Usage: create 'data' folder in 'bert' folder and copy downloaded .csv files from the link above.
    print('-------------------------------------------------------------------------------------------------')
    print('Loading data...')
    data = bertutils.load_and_preprocess_data()

    # 2. Split data into train, validation and test sets
    # Usage: create 'datasets' folder in 'bert' folder
    print('-------------------------------------------------------------------------------------------------')
    print('Creating train, validation and test data splits...')
    train_dataset, valid_dataset, test_t, test_l = bertutils.make_datasets(data)

    # 3. Instantiate and train model, or load fitted instance
    # Usage: create 'model' folder inside 'bert' folder, and copy provided model in new folder 'distilbert-base-uncased'
    # The trained model can be downloaded at the Google Drive link down below:
    # https://drive.google.com/drive/folders/1J-hDAyK4vcwK_fJ5tk_NUKQVuh6okIfa?usp=sharing
    # Model (DistilBERT_GPU_ADAM_LR2e-5_BCE_2EP_BS16_SHUFFLE_100):
    # - Trained with GPU P100 on Kaggle
    # - Optimizer: Adam with learning_rate=2e-5
    # - Loss Func: Binary Cross Entropy (BCE)
    # - Epochs: 2
    # - Batch size: 16
    # - Shuffle: 100
    print('-------------------------------------------------------------------------------------------------')
    model_file = 'bert/model/distilbert-base-uncased'
    if exists(model_file):
        print('Loading trained model...')
        model = TFDistilBertForSequenceClassification.from_pretrained(model_file)
    else:
        print('Training model...')
        model = bertutils.train_model(train_dataset, valid_dataset)

    # Evaluate model
    # Usage: set flag to True to evaluate model locally on CPU
    evaluate = True
    if evaluate:
        print('-------------------------------------------------------------------------------------------------')
        print('Start model evaluation...')
        model = bertutils.evaluate_model(model, test_t, test_l)

    # 4. Make predictions on unseen data with trained model on downstream task
    print('-------------------------------------------------------------------------------------------------')
    print('Make predictions with fitted model...')
    bertutils.make_predictions(model)


if __name__ == '__main__':
    print('Start sentiment analysis with DistilBERT...')
    sentiment_analysis_BERT()
    print('Finished sentiment analysis with DistilBERT.')
